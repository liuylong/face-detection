//
//  FaceCompareVC.h
//  catface
//
//  Created by fairy on 2017/8/30.
//  Copyright © 2017年 fairy. All rights reserved.
//

#import <UIKit/UIKit.h>
//简书-https://www.jianshu.com/p/6a56883d88a2
/*
 先列个大纲
 1.模板匹配-
 2.直方图比较-本例采用, 将包含人脸的图片作为基准图，然后与匹配图片做直方图比对
 3.感知哈希算法
 4.特征点匹配
 */

@interface FaceCompareVC : UIViewController


@end

