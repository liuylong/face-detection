//
//  FaceFeatureVC.m
//  FaceDetecDemo
//
//  Created by 徐国梁 on 2020/11/11.
//  Copyright © 2020 xiaohui mu. All rights reserved.
//

#import "FaceFeatureVC.h"
#import <Vision/Vision.h>

@interface FaceFeatureVC()
@property(nonatomic, strong) UIImageView* imageView;
@property(nonatomic, strong) UIImage *image;
@end

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

@implementation FaceFeatureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _image = [UIImage imageNamed:@"7"];
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, _image.size.height/_image.size.width*kScreenWidth)];
    _imageView.backgroundColor = UIColor.redColor;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.image= _image;
    [self.view addSubview:_imageView];
    
    CIImage *faceCIImage = [[CIImage alloc]initWithImage:_image];
    if (@available(iOS 11.0, *)) {
        //1
        VNImageRequestHandler *vnRequestHeader = [[VNImageRequestHandler alloc] initWithCIImage:faceCIImage options:@{}];
        //2
        __weak FaceFeatureVC *weakSelf = self;
        VNDetectFaceLandmarksRequest *faceRequest = [[VNDetectFaceLandmarksRequest alloc] initWithCompletionHandler:^(VNRequest * _Nonnull request, NSError * _Nullable error) {
            [weakSelf faceLandmarks:request.results];
        }];
        // 3
        [vnRequestHeader performRequests:@[faceRequest] error:NULL];
    } else {
        // Fallback on earlier versions
    }
    
    
}

// 获取信息成功后 处理
- (void)faceLandmarks:(NSArray *)faces{
    if (@available(iOS 11.0, *)) {
        // 可能是多张脸
        [faces enumerateObjectsUsingBlock:^(VNFaceObservation *face, NSUInteger idx, BOOL * _Nonnull stop) {
            
            /*
             * face: VNFaceObservation 对象, 里面包含了 landmarks 位置信息, boundingBox 脸的大小 等等信息
             */
            
            
            // 取出单个脸的 landmarks
            VNFaceLandmarks2D *landmarks = face.landmarks;
            // 声明一个存关键位置的数组
            NSMutableArray *face_landmarks = [NSMutableArray array];
            
            // landmarks 是一个对象，对象中有左眼位置，右眼，鼻子，鼻梁等等属性 根据需求自己添加
            [face_landmarks addObject:landmarks.faceContour];
            [face_landmarks addObject:landmarks.leftEye];
            [face_landmarks addObject:landmarks.rightEye];
            [face_landmarks addObject:landmarks.leftEyebrow];
            [face_landmarks addObject:landmarks.rightEyebrow];
            [face_landmarks addObject:landmarks.outerLips];
            [face_landmarks addObject:landmarks.innerLips];
            [face_landmarks addObject:landmarks.nose];
            [face_landmarks addObject:landmarks.noseCrest];
            [face_landmarks addObject:landmarks.medianLine];
            [face_landmarks addObject:landmarks.outerLips];
            [face_landmarks addObject:landmarks.innerLips];
            [face_landmarks addObject:landmarks.leftPupil];
            [face_landmarks addObject:landmarks.rightPupil];
            
            // 当前脸在图片中的位置和大小
            // 注意: boundingBox 返回的x,y,w,h 的比例 不是直接的值，所以需要转换
            
            // 这里的 image_wh是一个宏 表示的图片的宽高大小 也可以直接image.size.width....
            CGFloat image_w = kScreenWidth;//_image.size.width;
            CGFloat image_h = _imageView.frame.size.height;
            CGFloat faceRectWidth = image_w * face.boundingBox.size.width;
            CGFloat faceRectHeight = image_h * face.boundingBox.size.height;
            CGFloat faceRectX = face.boundingBox.origin.x * image_w;
            // Y默认的位置是左下角
            CGFloat faceRectY = face.boundingBox.origin.y * image_h;
            
            // 遍历位置信息
            [face_landmarks enumerateObjectsUsingBlock:^(VNFaceLandmarkRegion2D *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                // VNFaceLandmarkRegion2D *obj 是一个对象. 表示当前的一个部位
                // 遍历当前部分所有的点
                for (int i=0; i<obj.pointCount; i++) {
                    // 取出点
                    CGPoint point = obj.normalizedPoints[i];
                    
                    // 计算出center
                    /*
                     * 这里的 point 的 x,y 表示也比例, 表示当前点在脸的比例值
                     * 因为Y点是在左下角， 所以我们需要转换成左上角
                     * 这里的center 关键点 可以根据需求保存起来
                     */
                    CGPoint center = CGPointMake(faceRectX + faceRectWidth * point.x,  image_h -
                                                 (faceRectY + faceRectHeight * point.y));
                    
                    // 将点显示出来
                    UIView *point_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3, 3)];
                    point_view.backgroundColor = [UIColor redColor];
                    point_view.center = center;
                    // 将点添加到imageView上即可 需要注意，当前image的bounds 应该和图片大小一样大
                    [_imageView addSubview:point_view];
                    
                }
            }];
            
        }];
    }else {
        
    }
}

@end
