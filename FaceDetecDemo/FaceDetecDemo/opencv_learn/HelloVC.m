//
//  HelloVC.m
//  FaceDetecDemo
//
//  Created by 徐国梁 on 2020/11/4.
//  Copyright © 2020 xiaohui mu. All rights reserved.
//

#import "HelloVC.h"
#import "opencvUtil.h"

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

@interface HelloVC ()

@end

@implementation HelloVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Hello!" message:@"Welcome to OpenCV" delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
//    [alert show];
    
    UIImageView *imgView1=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight/3)];
    UIImage *image = [UIImage imageNamed:@"origin"];
    imgView1.image= image;
    [self.view addSubview:imgView1];
    
    UIImageView *imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(0, kScreenHeight/3, kScreenWidth, kScreenHeight/3)];
    UIImage *image2 = [[opencvUtil new] grayImage:image];
    imgView2.image= image2;
    [self.view addSubview:imgView2];
}

@end
