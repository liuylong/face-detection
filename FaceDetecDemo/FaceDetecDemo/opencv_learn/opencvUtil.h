//
//  opencvUtil.h
//  FaceDetecDemo
//
//  Created by 徐国梁 on 2020/11/4.
//  Copyright © 2020 xiaohui mu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface opencvUtil : NSObject
-(UIImage *)findFaceFrom:(UIImage *)image;
-(UIImage *)grayImage:(UIImage *)img;
@end

NS_ASSUME_NONNULL_END
