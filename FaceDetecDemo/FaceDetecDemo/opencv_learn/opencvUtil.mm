//
//  opencvUtil.m
//  FaceDetecDemo
//
//  Created by 徐国梁 on 2020/11/4.
//  Copyright © 2020 xiaohui mu. All rights reserved.
//

#import "opencvUtil.h"
#import <opencv2/opencv.hpp>

@interface opencvUtil () {
    cv::CascadeClassifier _faceDetector;
    cv::vector<cv::Rect> _faceRects;
    cv::vector<cv::Mat> _faceImgs;
}

@end
@implementation opencvUtil
//MARK: - 返回图片中的人脸图
- (UIImage *)findFaceFrom:(UIImage *)image {
    //预设置face探测的参数
    [self preSetFace];
    //image转mat
    cv::Mat mat = [self cvMatFromUIImage: image];
    return [self processImage:mat];
}


- (void)preSetFace {
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt" ofType:@"xml"];
    
    const CFIndex CASCADE_NAME_LEN = 2048;
    char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
    CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
    
    _faceDetector.load(CASCADE_NAME);
    
    free(CASCADE_NAME);
}

- (UIImage *)processImage:(cv::Mat&)inputImage {
    // Do some OpenCV stuff with the image
    cv::Mat frame_gray;

    //转换为灰度图像
    cv::cvtColor(inputImage, frame_gray, CV_BGR2GRAY);
    
    //图像均衡化
    cv::equalizeHist(frame_gray, frame_gray);

    //分类器识别
    _faceDetector.detectMultiScale(frame_gray, _faceRects,1.1,2,0,cv::Size(30,30));

    cv::vector<cv::Rect> faces;
    faces = _faceRects;
    
    cv::Mat image1;
    if (faces.size()>0) {
        cv::Point center(faces[0].x + faces[0].width / 2, faces[0].y + faces[0].height / 2);
        //请注意 inputImage(cv::Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height)) 不是副本，而是指 image 中的相同像素。如果你想要一个副本，最后添加一个.clone（）。
        image1 = inputImage(cv::Rect(faces[0].x, faces[0].y, faces[0].width, faces[0].height)).clone();
        return [self UIImageFromCVMat:image1];
    }else {
        return nil;
    }
}

//MARK: 转成Mat 转回UIImage后无变化
- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    return cvMat;
}

//MARK: - mat转UIImage
-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    return finalImage;
}









-(UIImage *)grayImage:(UIImage *)img {
    cv::Mat inputMat = [self cvMatFromUIImage:img];
    cv::Mat greyMat;
    cv::cvtColor(inputMat, greyMat, cv::COLOR_BGR2GRAY);//去掉彩色
    return [self UIImageFromCVMat:greyMat];
}
//MARK: 转成Mat 转回UIImage后无变化
//- (cv::Mat)cvMatFromUIImage:(UIImage *)image
//{
//    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
//    CGFloat cols = image.size.width;
//    CGFloat rows = image.size.height;
//    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
//    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
//                                                    cols,                       // Width of bitmap
//                                                    rows,                       // Height of bitmap
//                                                    8,                          // Bits per component
//                                                    cvMat.step[0],              // Bytes per row
//                                                    colorSpace,                 // Colorspace
//                                                    kCGImageAlphaNoneSkipLast |
//                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
//    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
//    CGContextRelease(contextRef);
//    return cvMat;
//}
- (cv::Mat)cvMatGrayFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    return cvMat;
}
//MARK: - mat转UIImage
//-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
//{
//    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
//    CGColorSpaceRef colorSpace;
//    if (cvMat.elemSize() == 1) {
//        colorSpace = CGColorSpaceCreateDeviceGray();
//    } else {
//        colorSpace = CGColorSpaceCreateDeviceRGB();
//    }
//    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
//    // Creating CGImage from cv::Mat
//    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
//                                        cvMat.rows,                                 //height
//                                        8,                                          //bits per component
//                                        8 * cvMat.elemSize(),                       //bits per pixel
//                                        cvMat.step[0],                            //bytesPerRow
//                                        colorSpace,                                 //colorspace
//                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
//                                        provider,                                   //CGDataProviderRef
//                                        NULL,                                       //decode
//                                        false,                                      //should interpolate
//                                        kCGRenderingIntentDefault                   //intent
//                                        );
//    // Getting UIImage from CGImage
//    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);
//    CGDataProviderRelease(provider);
//    CGColorSpaceRelease(colorSpace);
//    return finalImage;
//}

//MARK: -下面的多头像检测并用红色四方形框出

//- (void)preSetFace {
//    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt2" ofType:@"xml"];
//
//    const CFIndex CASCADE_NAME_LEN = 2048;
//    char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
//    CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
//
//    _faceDetector.load(CASCADE_NAME);
//
//    free(CASCADE_NAME);
//}
//
//- (void)processImage:(cv::Mat&)inputImage {
//    // Do some OpenCV stuff with the image
//    cv::Mat frame_gray;
//
//    //转换为灰度图像
//    cv::cvtColor(inputImage, frame_gray, CV_BGR2GRAY);
//
//    //图像均衡化
//    cv::equalizeHist(frame_gray, frame_gray);
//
//    //分类器识别
//    _faceDetector.detectMultiScale(frame_gray, _faceRects,1.1,2,0,cv::Size(30,30));
//
//    vector<cv::Rect> faces;
//    faces = _faceRects;
//
//    // 在每个人脸上画一个红色四方形
//    for(unsigned int i= 0;i < faces.size();i++)
//    {
//        const cv::Rect& face = faces[i];
//        cv::Point tl(face.x,face.y);
//        cv::Point br = tl + cv::Point(face.width,face.height);
//        // 四方形的画法
//        cv::Scalar magenta = cv::Scalar(255, 0, 0, 255);
//        cv::rectangle(inputImage, tl, br, magenta, 3, 8, 0);
//    }
//    UIImage *outputImage = MatToUIImage(inputImage);
//    self.imageView.image = outputImage;
//}

@end
