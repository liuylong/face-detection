//
//  ViewController.swift
//  MyTest
//
//  Created by edz on 2019/4/28.
//  Copyright © 2019年 edz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, YLRectDetectorVCDelegage {
      
    private var iv: UIImageView!
    
    let screenWidth = UIScreen.main.bounds.width
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iv = UIImageView(frame:CGRect(x: (screenWidth-300)/2, y: 100, width: 300, height: 300))
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .red
        view.addSubview(iv)
        
        let btn = UIButton(frame:CGRect(x: (screenWidth-200)/2, y: iv.frame.maxY+40, width: 200, height: 30))
        btn.setTitle("打开", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btn.setTitleColor(.blue, for: .normal)
        btn.addTarget(self, action: #selector(open), for: .touchUpInside)
        view.addSubview(btn)
    }
    
    /// 打开 App
    @objc func open() {
        let vc = YLRectDetectorVC()
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    //代理
    func rectDetectorVCDidTakePhoto(image: UIImage) {
        iv.image = image
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
