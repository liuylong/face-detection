//
//  YLRectDetectorVC.swift
//  lyb
//
//  Created by yl on 2020/9/10.
//  Copyright © 2020 edz. All rights reserved.
//

import UIKit

protocol YLRectDetectorVCDelegage: NSObjectProtocol {
    func rectDetectorVCDidTakePhoto(image: UIImage)
}

class YLRectDetectorVC: UIViewController {
    
    weak var delegate: YLRectDetectorVCDelegage?
    private var cameraCaptureView: YLCameraCaptureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cameraCaptureView.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //cameraCaptureView.stop()
    }
    
    private func addSubviews() {
        cameraCaptureView = YLCameraCaptureView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        //打开边缘检测
        cameraCaptureView.enableBorderDetection = true
        cameraCaptureView.backgroundColor = .black
        view.addSubview(cameraCaptureView)
        cameraCaptureView.setupCameraView()
        //        let tap = UITapGestureRecognizer(target: self, action: #selector())
        //        [self.captureCameraView addGestureRecognizer:self.tapGestureRecognizer];
        //        [self.tapGestureRecognizer addTarget: self action: @selector(handleTapGesture:)];
        
        let snapshotBtn = UIButton(frame: CGRect(x: (kScreenWidth-80)/2, y: kScreenHeight-100, width: 80, height: 80))
        snapshotBtn.backgroundColor = .white
        snapshotBtn.makeCircleCorner(40, 4, kColor(255, 255, 255, 1).cgColor)
        snapshotBtn.addTarget(self, action: #selector(onSnapshotBtn), for: .touchUpInside)
        view.addSubview(snapshotBtn)
        
        // 外圆圈
        //        let layer = CAShapeLayer()
        //        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 80, height: 80))
        //        //UIColor.white.withAlphaComponent(0.5).setStroke()
        //        layer.path = path.cgPath
        //        layer.fillColor = UIColor.white.cgColor
        //        snapshotBtn.layer.addSublayer(layer)
        
        //        // 外圆圈
        //        let outLayer = CAShapeLayer()
        //        let outPath = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 80, height: 80))
        //        outLayer.path = outPath.cgPath
        //        outLayer.fillColor = UIColor.white.withAlphaComponent(0.5).cgColor
        //        snapshotBtn.layer.addSublayer(outLayer)
        //        // 内圆
        //        let innerLayer = CAShapeLayer()
        //        let innerPath = UIBezierPath(ovalIn: CGRect(x: 10, y: 10, width: 60, height: 60))
        //        innerLayer.path = innerPath.cgPath
        //        outLayer.fillColor = UIColor.white.cgColor
        //        snapshotBtn.layer.addSublayer(innerLayer)
    }
    
    @objc func onSnapshotBtn(sender: UIButton) {
        cameraCaptureView.captureImageWithCompletionHandler { (image, borderDetectFeature) in
            DispatchQueue.main.async {
                if self.delegate != nil {
                    self.delegate?.rectDetectorVCDidTakePhoto(image: image)
                    self.dismiss(animated: false, completion: nil)
                }
            }
            
            //.iv.image = image
            //            __strong typeof(self) strongSelf = weakSelf;
            //
            //            MADCropScaleController *vc = [[MADCropScaleController alloc] init];
            //            vc.borderDetectFeature = borderDetectFeature;
            //            vc.cropImage = data;
            //            [strongSelf presentViewController:vc animated:YES completion:nil];
        }
    }
    
    //    @objc func handleTapGesture(sender: UITapGestureRecognizer) {
    //        if (sender.state == .recognized) {
    //            let location = sender.location(in: view)
    //            cameraCaptureView.focusAtPoint(point: location) { [weak self]
    //
    //            }
    //            [self.captureCameraView focusAtPoint:location completionHandler:^
    //             {
    //                 [self focusIndicatorAnimateToPoint:location];
    //             }];
    //            [self focusIndicatorAnimateToPoint:location];
    //        }
    //    }
    //
    //    private func focusIndicatorAnimateToPoint(targetPointtargetPoint: CGPoint) {
    //        [self.focusIndicator setCenter:targetPoint];
    //        self.focusIndicator.alpha = 0.0;
    //        self.focusIndicator.hidden = NO;
    //
    //        [UIView animateWithDuration:0.4 animations:^
    //         {
    //             self.focusIndicator.alpha = 1.0;
    //         }
    //                         completion:^(BOOL finished)
    //         {
    //             [UIView animateWithDuration:0.4 animations:^
    //              {
    //                  self.focusIndicator.alpha = 0.0;
    //              }];
    //         }];
    //    }
}

extension UIView {
    func makeCircleCorner(_ cornerRadius: CGFloat, _ borderWidth: CGFloat, _ borderColor: CGColor) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
}
