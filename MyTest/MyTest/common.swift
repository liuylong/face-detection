//
//  common.swift
//  MyTest
//
//  Created by 徐国梁 on 2020/9/11.
//  Copyright © 2020 edz. All rights reserved.
//

import Foundation
import UIKit

//MARK: - 宽高距离
let kScreenHeight = UIScreen.main.bounds.height
//屏幕宽度
let kScreenWidth = UIScreen.main.bounds.width
//内容边距
let contentMargin: CGFloat = 8.0
//页边距
let pageMargin: CGFloat = 15.0
///状态栏高度
let kStatusBarHeight = UIApplication.shared.statusBarFrame.height
///有导航栏的安全区顶部高度
let kSafeAreaTopHeight = CGFloat(UIApplication.shared.statusBarFrame.height+44.0)
///tabbar高度
let kTabBarHeight = CGFloat(kStatusBarHeight == 44.0 ? 83.0 : 49.0)
///安全区底部高度
let kSafeAreaBottomHeight = CGFloat(kTabBarHeight - 49.0)
///有导航栏的安全区高度-iPhoneX适配
let kSafeAreaHeight = kScreenHeight-kSafeAreaTopHeight-kSafeAreaBottomHeight
///没有导航栏的安全区高度
let kNoNavigationBarSafeAreaHeight = kScreenHeight-kStatusBarHeight-kSafeAreaBottomHeight
///有tabbar的安全区高度
let kTabBarSafeAreaHeight = kScreenHeight-kSafeAreaTopHeight-kTabBarHeight

//MARK: - 常用方法
//生成颜色RGB
func kColor(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}
