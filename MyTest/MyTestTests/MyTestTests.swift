//
//  MyTestTests.swift
//  MyTestTests
//
//  Created by edz on 2019/4/28.
//  Copyright © 2019年 edz. All rights reserved.
//

import XCTest
//@testable import MyTest

class MyTestTests: XCTestCase {
    
    
    override func setUp() {
        super.setUp()
        
        print("starting test ")
        
        print("使用这个类中的每个测试方法前都会调用该方法,可以在这里插入对应的全局设置代码")
        
    }

    override func tearDown() {
        super.tearDown()
        
        print("end test")
        print("使用这个类中的每个测试方法后都会调用该方法,可以在这里插入全局配置代码 -如清理设置等")
    }
    
//    func testAdd() {
//        let result = mySimpleInterestCaculator.calculateAdd(num1: 12, num2: 12)
//        // accuracy 表示 精确性
//        XCTAssertEqual(result, 24.01, accuracy: 0.1, "error: Unexpected result->\(result)")
//    }
//
//    func testSimpleInterest() {
//        let result = mySimpleInterestCaculator.calculate(loanAmount: 25_000, interestRate: 0.08, years: 10)
//        XCTAssertEqual(result, 25200, accuracy: 0.1, "error: Unexpected result->\(result)")
//
//    }
    

    func testExample() {
        let a = 3
        XCTAssertTrue(a == 3, "a不能等于0")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    /**
     XCTFail(format…) 生成一个失败的测试
     XCTAssertNil(a1, format...)为空判断，a1为空时通过，反之不通过
     XCTAssertNotNil(a1, format…)不为空判断，a1不为空时通过，反之不通过
     XCTAssert(expression, format...)当expression求值为TRUE时通过
     XCTAssertTrue(expression, format...)当expression求值为TRUE时通过
     XCTAssertFalse(expression, format...)当expression求值为False时通过
     XCTAssertEqualObjects(a1, a2, format...)判断相等，[a1 isEqual:a2]值为TRUE时通过，其中一个不为空时，不通过
     XCTAssertNotEqualObjects(a1, a2, format...)判断不等，[a1 isEqual:a2]值为False时通过；
     
     XCTAssertEqual(a1, a2, format...)判断相等（当a1和a2是 C语言标量、结构体或联合体时使用, 判断的是变量的地址，如果地址相同则返回TRUE，否则返回NO）；
     
     XCTAssertNotEqual(a1, a2, format...)判断不等（当a1和a2是 C语言标量、结构体或联合体时使用）；
     
     XCTAssertEqualWithAccuracy(a1, a2, accuracy, format...)判断相等，（double或float类型）提供一个误差范围，当在误差范围（+/-accuracy）以内相等时通过测试；
     
     XCTAssertNotEqualWithAccuracy(a1, a2, accuracy, format...) 判断不等，（double或float类型）提供一个误差范围，当在误差范围以内不等时通过测试；
     
     XCTAssertThrows(expression, format...)异常测试，当expression发生异常时通过；反之不通过；（很变态） XCTAssertThrowsSpecific(expression, specificException, format...) 异常测试，当expression发生specificException异常时通过；反之发生其他异常或不发生异常均不通过；
     
     XCTAssertThrowsSpecificNamed(expression, specificException, exception_name, format...)异常测试，当expression发生具体异常、具体异常名称的异常时通过测试，反之不通过；
     
     XCTAssertNoThrow(expression, format…)异常测试，当expression没有发生异常时通过测试；
     
     XCTAssertNoThrowSpecific(expression, specificException, format...)异常测试，当expression没有发生具体异常、具体异常名称的异常时通过测试，反之不通过；
     
     XCTAssertNoThrowSpecificNamed(expression, specificException, exception_name, format...)异常测试，当expression没有发生具体异常、具体异常名称的异常时通过测试，反之不通过
     
     记不住这么多？没关系，记住这个就好了
     XCTAssert(expression, format...)当expression求值为TRUE时通过
     **/
}
