//
//  UrlTests.swift
//  MyTestTests
//
//  Created by edz on 2019/5/30.
//  Copyright © 2019年 edz. All rights reserved.
//

import XCTest

class UrlTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    
    func testShouldGetCorrectPathWhenNoSegmentProvided() {
//        let resourcePath = "customers"
//
//        let result = urlInstance.getActualPathFrom(resourcePath: resourcePath, segments: [String:String]())
//
//        XCTAssertEqual(result, "http://localhost:8080/api/customers", "Can not get corrent path when no segments provided")
    }
    
    func testGetCorrectPathGivenOneSegment() {
//        let resourcePath = "customer/{id}"
//
//        let result = urlInstance.getActualPathFrom(resourcePath: resourcePath, segments: ["id": "10"]);
//
//        XCTAssertEqual(result, "http://localhost:8080/api/customer/10", "Can not get corrent path when only one segment provided")
    }
}
